from flask import Flask
from flask_restx import Api, Resource, fields


# APP

app = Flask(__name__)
api = Api(app, version='1.0', title='PaymentSystem API', description='A simple payment API')


# DAO

class InvoiceDAO(object):
    def __init__(self):
        self._counter = 0
        self.invoices = []

    def get(self, id):
        for invoice in self.invoices:
            if invoice['id'] == id:
                return invoice
        api.abort(404, f"There is no invoice with ID {id}")

    def create(self, data):
        invoice = data
        invoice['id'] = self._counter = self._counter + 1
        self.invoices.append(invoice)
        return invoice

    def update(self, id, data):
        invoice = self.get(id)
        invoice.update(data)
        return invoice


DAO = InvoiceDAO()

# INVOICE

nsi = api.namespace('invoice', description='invoice operations')

invoice = api.model('Invoice', {
    'id': fields.Integer(readonly=True, description='The invoice unique identifier'),
    'sender': fields.String(required=True, description='The sender of the invoice'),
    'receiver': fields.String(required=True, description='The receiver of the invoice'),
    'total_amount': fields.Float(required=True, description='Total invoice amount of money owed'),
    'outstanding_amount': fields.Float(required=True, description='Current invoice amount of money owed'),
    # ToDo improvement handle decimals by rounding to two decimal places for money types and add currency
    })


@nsi.route('')
class Invoices(Resource):
    @nsi.doc('List all invoices')
    @nsi.marshal_list_with(invoice)
    def get(self):
        """ List invoices """
        return DAO.invoices

    @nsi.doc('create an invoice')
    @nsi.expect(invoice)
    @nsi.marshal_with(invoice, code=201)
    def post(self):
        """ Create new invoice """
        return DAO.create(api.payload), 201


@nsi.route('/<int:id>')
@nsi.response(404, 'Invoice not found')
@nsi.param('id', 'Invoice id')
class Invoice(Resource):
    @nsi.doc('get an invoice')
    @nsi.marshal_with(invoice)
    def get(self, id):
        """ Fetch an invoice """
        return DAO.get(id)

    @nsi.expect(invoice)
    @nsi.marshal_with(invoice)
    def put(self, id):
        """ Update an invoice """
        return DAO.update(id, api.payload)


# PAYMENTS

nsp = api.namespace('payment', description='payment operations')

payment = api.model('Payment', {
    'id': fields.Integer(readonly=True, description='The payment unique identifier'),
    'amount': fields.Float(required=True, description='Amount of money'),
})


@nsp.route('')
class Payment(Resource):
    ...


if __name__ == '__main__':
    app.run(debug=True)
